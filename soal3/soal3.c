// Soal 3 Modul 2
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#include <fcntl.h>


// function to make directory 
void make_dir(char where[]){
	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id<0){
		exit(EXIT_FAILURE); 
	}
	if (child_id==0){
		execlp("mkdir", "mkdir", "-p", where, NULL);
	}
	else{
        	((wait(&status))>0);
    }
}

// function to unzip file
void unzip_file(char bash[], char *command[]){
	pid_t child_id;
	int status;
	child_id = fork();
    
	if (child_id<0) {
		exit(EXIT_FAILURE); 
	}
	if(child_id==0){
		execv(bash, command);
	}
	else{
		((wait(&status))>0);
	}
}

// MAIN PROGRAM
int main(){
	// MAKE DIRECTORY DARAT & AIR
	make_dir("/home/nde/modul2/darat");
	
	sleep(3);
	make_dir("/home/nde/modul2/air");

	// UNZIP FILE ANIMAL.ZIP
	char *argv_animal[] =  {"unzip", "animal.zip", "-d", "modul2", NULL};
	unzip_file("/usr/bin/unzip", argv_animal);


	// CATEGORIZE DARAT & AIR, SISANYA REMOVE

	// REMOVE ALL BIRD IN DARAT

	// MAKE LIST.TXT
	
}
