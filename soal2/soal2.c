/*
 * This program displays the names of all files in the current directory.
 */

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define INPUT_DIR "/home/salman/shift2/drakor"
#define OUTPUT_DIR "/home/salman/shift2/tmp"

void make_dir(char *dirname) {
  char* command = malloc(sizeof(char) * (strlen(dirname) + 10));
  sprintf(command, "mkdir -p %s", dirname);


  pid_t pid;
  if ((pid = fork()) == -1)
    perror("fork error");
  else if (pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    printf("Return not expected. Must be an execl() error.n");
  } else {
    // Wait for child to exit.
    int status;
    waitpid(pid, &status, 0);
  }
  
  free(command);
}


void copy_file(char *src, char *dest) {
  char* command = malloc(sizeof(char) * (strlen(src) + strlen(dest) + 64));
  sprintf(command, "cp -rf \"%s\" \"%s\"", src, dest);

  pid_t pid;
  if ((pid = fork()) == -1)
    perror("fork error");
  else if (pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    printf("Return not expected. Must be an execl() error.n");
  } else {
    // Wait for child to exit.
    int status;
    waitpid(pid, &status, 0);
  }

  free(command);
}


void move_file(char *src, char *dest) {
  char* command = malloc(sizeof(char) * (strlen(src) + strlen(dest) + 64));
  sprintf(command, "mv \"%s\" \"%s\"", src, dest);

  pid_t pid;
  if ((pid = fork()) == -1)
    perror("fork error");
  else if (pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    printf("Return not expected. Must be an execl() error.n");
  } else {
    // Wait for child to exit.
    int status;
    waitpid(pid, &status, 0);
  }

  free(command);
}

void delete_file(char* filename) {
  char* command = malloc(sizeof(char) * (strlen(filename) + 64));
  sprintf(command, "rm -rf \"%s\"", filename);

  pid_t pid;
  if ((pid = fork()) == -1)
    perror("fork error");
  else if (pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    printf("Return not expected. Must be an execl() error.n");
  } else {
    // Wait for child to exit.
    int status;
    waitpid(pid, &status, 0);
  }

  free(command);
}

/**
 * Separete the file name from extension.
 *
 * @param filename - the char buffer to store the file name.
 * @param filename_ext - the file name with extension, will not be modified.
 */
void separate_filename(char *filename, char *filename_ext)
{
  strcpy(filename, filename_ext);

  char *ext = strrchr(filename, '.');
  if (ext != NULL)
  {
    *ext = '\0';
  }
}

void parse_information(char *filename_information, char *filename_ext)
{
  // Copy the parameter into new buffer.
  char *filename = malloc(sizeof(char) * (strlen(filename_information) + 1));
  strcpy(filename, filename_information);

  char *information[3];

  // Tokenize the filename_information.
  char *token = strtok(filename, ";");
  int i = 0;
  while (token != NULL)
  {
    information[i] = token;
    token = strtok(NULL, ";");
    i++;
  }

  // Create the directory.
  char *dirname = malloc(sizeof(char) * (strlen(information[0]) + 20));
  sprintf(dirname, "%s/%s", OUTPUT_DIR, information[2]);

  make_dir(dirname);

  // Copy the file to the new directory.
  char *src = malloc(sizeof(char) * (strlen(filename_ext) + 64));
  sprintf(src, "%s/%s", INPUT_DIR, filename_ext);
  
  char *dest = malloc(sizeof(char) * (strlen(dirname) + strlen(information[0]) + strlen(information[1]) + 64));
  sprintf(dest, "%s/%s;%s.png", dirname, information[1], information[0]);

  copy_file(src, dest);

  // TODO: Remove src.

  // Free the memory.
  free(filename);
  free(dirname);
  free(src);
  free(dest);
}

/**
 * Parse the name
 *
 * @param filename_ext
 */
void parse_filename_information(char *filename_ext)
{
  char *filename;
  filename = (char *)malloc(sizeof(char) * 128);

  // Separate the filename from extension.
  separate_filename(filename, filename_ext);

  // Parse the information.
  char *movie_information[8];
  int num = 0;

  char *token = strtok(filename, "_");
  while (token != NULL)
  {
    movie_information[num] = token;
    token = strtok(NULL, "_");
    num++;
  }

  for (int i = 0; i < num; i++) {
    parse_information(movie_information[i], filename_ext);
  }

  free(filename);
}

void process_png_files(char *input_dirname)
{
  DIR *dir;
  struct dirent *entry;
  char *ext;

  // Open the directory.
  if (!(dir = opendir(input_dirname)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Check if its a file.
    if (entry->d_type != DT_REG) {
      continue;
    }

    // Get the extension of the file.
    ext = strrchr(entry->d_name, '.');

    if (ext && !strcmp(ext, ".png"))
    {
      parse_filename_information(entry->d_name);
    }
  }

  closedir(dir);
}



/**
 * Parse the name
 *
 * @param filename_ext
 */
void parse_genre_information(char *filename_ext, char* genre_dirname)
{
  char *filename;
  filename = (char *)malloc(sizeof(char) * 128);

  // Separate the filename from extension.
  separate_filename(filename, filename_ext);

  char *information[2];

  // Tokenize the filename.
  char *token = strtok(filename, ";");
  int i = 0;
  while (token != NULL)
  {
    information[i] = token;
    token = strtok(NULL, ";");
    i++;
  }

  // Create information files.
  FILE *fp;
  char* data_dirname = malloc(sizeof(char) * (strlen(genre_dirname) + strlen(information[0]) + 64));
  sprintf(data_dirname, "%s/%s", genre_dirname, "data.txt");
  fp = fopen (data_dirname, "a");

  // Add information about the movie.
  fprintf(fp, "\n");
  fprintf(fp, "nama: %s\n", information[1]);
  fprintf(fp, "rilis: tahun %s\n", information[0]);

  fclose(fp);

  // Change the filename to the new format.
  char *new_filename = malloc(sizeof(char) * (strlen(information[0]) + strlen(information[1]) + 64));
  sprintf(new_filename, "%s.png", information[1]);

  char *src = malloc(sizeof(char) * (strlen(genre_dirname) + strlen(filename_ext) + 64));
  sprintf(src, "%s/%s", genre_dirname, filename_ext);
  char *dest = malloc(sizeof(char) * (strlen(genre_dirname) + strlen(new_filename) + 64));
  sprintf(dest, "%s/%s", genre_dirname, new_filename);
  move_file(src, dest);
  
  free(filename);
}

void process_genre_png_files(char* genre, char *genre_dirname)
{
  // Create data.txt file.
  FILE *fp;
  char* data_dirname = malloc(sizeof(char) * (strlen(genre_dirname) + strlen("data.txt") + 64));
  sprintf(data_dirname, "%s/%s", genre_dirname, "data.txt");
  printf("%s\n", data_dirname);

  fp = fopen (data_dirname, "w+");
  // Add information about genre.
  char* genre_name = malloc(sizeof(char) * (strlen(genre) + 64));
  sprintf(genre_name, "kategori: %s", genre); 
  fprintf(fp, "%s\n", genre_name);

  fclose(fp);

  DIR *dir;
  struct dirent *entry;
  char *ext;

  // Open the directory.
  if (!(dir = opendir(genre_dirname)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Check if its a file.
    if (entry->d_type != DT_REG) {
      continue;
    }

    // Get the extension of the file.
    ext = strrchr(entry->d_name, '.');

    if (ext && !strcmp(ext, ".png"))
    {
      parse_genre_information(entry->d_name, genre_dirname);
    }
  }

  closedir(dir);
}

void process_genre_dir(char* output_dirname) {
  DIR *dir;
  struct dirent *entry;
  char *ext;

  char* genre[32];
  int num_genre = 0;

  // Open the directory.
  if (!(dir = opendir(output_dirname)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Check if its a directory.
    if (entry->d_type != DT_DIR) {
      continue;
    }
    // Check if the directory is a genre directory.
    if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
      genre[num_genre] = entry->d_name;
      num_genre++;
    }
    
  }

  closedir(dir);

  // Process the genre directories.
  for (int i = 0; i < num_genre; i++) {
    char* genre_dirname = malloc(sizeof(char) * (strlen(output_dirname) + strlen(genre[i]) + 2));
    sprintf(genre_dirname, "%s/%s", output_dirname, genre[i]);
    process_genre_png_files(genre[i], genre_dirname);
    free(genre_dirname);
  }
}

void process_finish_png_files(char* input_dirname, char* output_dirname) {
  // Delete the input dir.
  delete_file(input_dirname);

  // Move tmp to input.
  move_file(output_dirname, input_dirname);
}

int main(void)
{
  process_png_files(INPUT_DIR);
  process_genre_dir(OUTPUT_DIR);
  process_finish_png_files(INPUT_DIR, OUTPUT_DIR);

}