# soal-shift-sisop-modul-2-ITB10-2022

## Soal 1

##### Oleh Romandhika Rijal Ibrahim (AFK)

## Soal 2

##### Oleh Salman Al Farisi Sudirlan (5027201056)

### Deklarasi

Pertama saya deklarasikan dulu fungsi yang ingin dipaka untuk memenuhi program saya, lalu saya define jalur input dan output agar tidak lagi menulis jalurnya.

```c
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define INPUT_DIR "/home/salman/shift2/drakor"
#define OUTPUT_DIR "/home/salman/shift2/tmp"
```

### Buat direktori

Untuk membuat direktori cara saya sama seperti di modul dengan menggunakan pid dan fork. 

```c
void make_dir(char *dirname) {
  char* command = malloc(sizeof(char) * (strlen(dirname) + 10));
  sprintf(command, "mkdir -p %s", dirname);


  pid_t pid;
  if ((pid = fork()) == -1)
    perror("fork error");
  else if (pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    printf("Return not expected. Must be an execl() error.n");
  } else {
    // Wait for child to exit.
    int status;
    waitpid(pid, &status, 0);
  }
  
  free(command);
}
```

### Move dan Delete File

Untuk delete dan copy sebenarnya sama caranya seperti di atas hanya cp-nya diganti jadi "mv" jika pindah dan "rm" jika delete.

```
void move_file(char *src, char *dest) {
  char* command = malloc(sizeof(char) * (strlen(src) + strlen(dest) + 64));
  sprintf(command, "mv \"%s\" \"%s\"", src, dest);

  pid_t pid;
  if ((pid = fork()) == -1)
    perror("fork error");
  else if (pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    printf("Return not expected. Must be an execl() error.n");
  } else {
    // Wait for child to exit.
    int status;
    waitpid(pid, &status, 0);
  }

  free(command);
}
```
```
void delete_file(char* filename) {
  char* command = malloc(sizeof(char) * (strlen(filename) + 64));
  sprintf(command, "rm -rf \"%s\"", filename);

  pid_t pid;
  if ((pid = fork()) == -1)
    perror("fork error");
  else if (pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    printf("Return not expected. Must be an execl() error.n");
  } else {
    // Wait for child to exit.
    int status;
    waitpid(pid, &status, 0);
  }

  free(command);
}
```

### Separate file name

untuk memisahkannya kita ada dua variabel filename untuk tempat dan filename_ext untuk lalu copy dan pisahkan dengan kode dibawah strcpy.

```
void separate_filename(char *filename, char *filename_ext)
{
  strcpy(filename, filename_ext);

  char *ext = strrchr(filename, '.');
  if (ext != NULL)
  {
    *ext = '\0';
  }
}
```
### Parse Information

Lalu selanjutnya fungsi untuk mengurai informasi dengan membuat direktori filenya lalu mengcopynya ke sesuai kateori

```
void parse_information(char *filename_information, char *filename_ext)
{
  // Copy the parameter into new buffer.
  char *filename = malloc(sizeof(char) * (strlen(filename_information) + 1));
  strcpy(filename, filename_information);

  char *information[3];

  // Tokenize the filename_information.
  char *token = strtok(filename, ";");
  int i = 0;
  while (token != NULL)
  {
    information[i] = token;
    token = strtok(NULL, ";");
    i++;
  }

  // Create the directory.
  char *dirname = malloc(sizeof(char) * (strlen(information[0]) + 20));
  sprintf(dirname, "%s/%s", OUTPUT_DIR, information[2]);

  make_dir(dirname);

  // Copy the file to the new directory.
  char *src = malloc(sizeof(char) * (strlen(filename_ext) + 64));
  sprintf(src, "%s/%s", INPUT_DIR, filename_ext);
  
  char *dest = malloc(sizeof(char) * (strlen(dirname) + strlen(information[0]) + strlen(information[1]) + 64));
  sprintf(dest, "%s/%s;%s.png", dirname, information[1], information[0]);

  copy_file(src, dest);

  // Free the memory.
  free(filename);
  free(dirname);
  free(src);
  free(dest);
}
```
### Parse File Name Information

Fungsi ini digunakan agar mendapat info tentang movienya dengan menggunakan separete_filename yang di atas untuk mendapatkan infonya lalu diolah kembali 

```
void parse_filename_information(char *filename_ext)
{
  char *filename;
  filename = (char *)malloc(sizeof(char) * 128);

  // Separate the filename from extension.
  separate_filename(filename, filename_ext);

  // Parse the information.
  char *movie_information[8];
  int num = 0;

  char *token = strtok(filename, "_");
  while (token != NULL)
  {
    movie_information[num] = token;
    token = strtok(NULL, "_");
    num++;
  }

  for (int i = 0; i < num; i++) {
    parse_information(movie_information[i], filename_ext);
  }

  free(filename);
}
```
### Process File PNG

Lalu untuk memproses file png-nya untuk mendapatkan tentang informasi file gambar movienya dengan bantuan fungsi di atas.

```
void process_png_files(char *input_dirname)
{
  DIR *dir;
  struct dirent *entry;
  char *ext;

  // Open the directory.
  if (!(dir = opendir(input_dirname)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Check if its a file.
    if (entry->d_type != DT_REG) {
      continue;
    }

    // Get the extension of the file.
    ext = strrchr(entry->d_name, '.');

    if (ext && !strcmp(ext, ".png"))
    {
      parse_filename_information(entry->d_name);
    }
  }

  closedir(dir);
}
```

### Parse Genre Information dan Process Genre PNG FILES

Fungsi ini berguna untuk mendapatkan genre dari suatu gambar setelah mendapat informasi dari fungsi yang saya tulis dibawah lalu akan ditulis tentang movienya lalu dikirim ke txt yang file txt nya akan dikirim ke masing-masing genre

```C
void parse_genre_information(char *filename_ext, char* genre_dirname)
{
  char *filename;
  filename = (char *)malloc(sizeof(char) * 128);

  // Separate the filename from extension.
  separate_filename(filename, filename_ext);

  char *information[2];

  // Tokenize the filename.
  char *token = strtok(filename, ";");
  int i = 0;
  while (token != NULL)
  {
    information[i] = token;
    token = strtok(NULL, ";");
    i++;
  }

  // Create information files.
  FILE *fp;
  char* data_dirname = malloc(sizeof(char) * (strlen(genre_dirname) + strlen(information[0]) + 64));
  sprintf(data_dirname, "%s/%s", genre_dirname, "data.txt");
  fp = fopen (data_dirname, "a");

  // Add information about the movie.
  fprintf(fp, "\n");
  fprintf(fp, "nama: %s\n", information[1]);
  fprintf(fp, "rilis: tahun %s\n", information[0]);

  fclose(fp);

  // Change the filename to the new format.
  char *new_filename = malloc(sizeof(char) * (strlen(information[0]) + strlen(information[1]) + 64));
  sprintf(new_filename, "%s.png", information[1]);

  char *src = malloc(sizeof(char) * (strlen(genre_dirname) + strlen(filename_ext) + 64));
  sprintf(src, "%s/%s", genre_dirname, filename_ext);
  char *dest = malloc(sizeof(char) * (strlen(genre_dirname) + strlen(new_filename) + 64));
  sprintf(dest, "%s/%s", genre_dirname, new_filename);
  move_file(src, dest);
  
  free(filename);
}
```

Untuk yang di bawah ini berfungsi mencari tahu kategori movie pada file png-nya dengan bantuan fungsi di atas.

```c
void process_genre_png_files(char* genre, char *genre_dirname)
{
  // Create data.txt file.
  FILE *fp;
  char* data_dirname = malloc(sizeof(char) * (strlen(genre_dirname) + strlen("data.txt") + 64));
  sprintf(data_dirname, "%s/%s", genre_dirname, "data.txt");
  printf("%s\n", data_dirname);

  fp = fopen (data_dirname, "w+");
  // Add information about genre.
  char* genre_name = malloc(sizeof(char) * (strlen(genre) + 64));
  sprintf(genre_name, "kategori: %s", genre); 
  fprintf(fp, "%s\n", genre_name);

  fclose(fp);

  DIR *dir;
  struct dirent *entry;
  char *ext;

  // Open the directory.
  if (!(dir = opendir(genre_dirname)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Check if its a file.
    if (entry->d_type != DT_REG) {
      continue;
    }

    // Get the extension of the file.
    ext = strrchr(entry->d_name, '.');

    if (ext && !strcmp(ext, ".png"))
    {
      parse_genre_information(entry->d_name, genre_dirname);
    }
  }

  closedir(dir);
}
```

### Proses dir, finishing, dan jalankan

Lalu fungsi dibawah proses untuk memasukkan genre ke folder yang akan dibuat sesuai setelah move jangan lupa delete yang lama.

```c
void process_genre_dir(char* output_dirname) {
  DIR *dir;
  struct dirent *entry;
  char *ext;

  char* genre[32];
  int num_genre = 0;

  // Open the directory.
  if (!(dir = opendir(output_dirname)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Check if its a directory.
    if (entry->d_type != DT_DIR) {
      continue;
    }
    // Check if the directory is a genre directory.
    if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
      genre[num_genre] = entry->d_name;
      num_genre++;
    }
    
  }

  closedir(dir);

  // Process the genre directories.
  for (int i = 0; i < num_genre; i++) {
    char* genre_dirname = malloc(sizeof(char) * (strlen(output_dirname) + strlen(genre[i]) + 2));
    sprintf(genre_dirname, "%s/%s", output_dirname, genre[i]);
    process_genre_png_files(genre[i], genre_dirname);
    free(genre_dirname);
  }
}

void process_finish_png_files(char* input_dirname, char* output_dirname) {
  // Delete the input dir.
  delete_file(input_dirname);

  // Move tmp to input.
  move_file(output_dirname, input_dirname);
}

int main(void)
{
  process_png_files(INPUT_DIR);
  process_genre_dir(OUTPUT_DIR);
  process_finish_png_files(INPUT_DIR, OUTPUT_DIR);

}
```

# Soal 3 
###### by Adinda Putri Audyna (5027201073)

## Soal

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang.

a. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

b. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

c. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

d. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

e. Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

Note:
- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec dan fork
- Direktori “.” dan “..” tidak termasuk


## Penyelesaian

#### a. Membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”

Untuk membuat directory darat dan air, saya membuat function `make_dir()` dengan parameter where1 yang menunjukkan dimana directory tersebut akan dibuat.
```c
// function to make directory 
void make_dir(char where1[]){
	pid_t child_id;
	int status;
	child_id = fork();
    
	if (child_id < 0) {
		exit(EXIT_FAILURE); 
	}
	if(child_id == 0){
		execlp("mkdir","mkdir","-p", where1, NULL);
	}
	else{
		while ((wait(&status))>0);
	}
}
```

Lalu pada main program, function tersebut dipanggil sesuai dengan permintaan soal, yaitu membuat directory `darat` lalu setelah 3 detik membuat directory `air`.
```c
// MAKE DIRECTORY DARAT & AIR
	int status=0;
	
	make_dir("/home/nde/modul2/darat");
	sleep(3);
	make_dir("/home/nde/modul2/air");
```


#### b. Extract “animal.zip” di “/home/[USER]/modul2/”

Untuk mengextract file animal.zip yang saya simpan di home, dengan menggunakan prinsip fork saya membuat function `unzip_file()` dengan parameter bash & command yang berisi sebagai berikut:
```c
// function to unzip file
void unzip_file(char bash[], char *command[]){
	pid_t child_id;
	int status;
	child_id = fork();
    
	if (child_id < 0) {
		exit(EXIT_FAILURE); 
	}
	if(child_id == 0){
		execv(bash, command);
	}
	else{
		((wait(&status))>0);
	}
}
```

Lalu pada main program, function tersebut dipanggil berdasarkan bash & command yang sesuai, yaitu dengan meng-unzip file animal.zip tersebut ke dalam folder modul2.
```c
	// UNZIP FILE ANIMAL.ZIP
	char *argv_animal[] =  {"unzip", "animal.zip", "-d", "modul2", NULL};
	unzip_file("/usr/bin/unzip", argv_animal);
```


#### c. Memisahkan hasil extract sesuai dengan nama filenya (darat/air), lalu hewan yang tidak ada keterangan air/darat dihapus
Pada bagian ini, saya membuat function utama yaitu function `categorize()` untuk mengkategorikan hewan darat dan air yang berada pada dir animal ke dalam dir darat dan air masing-masing dengan menggunakan function tambahan yaitu function `move_file()`. Untuk file yang tidak memiliki keterangan air/darat (dalam kasus soal ini yaitu file frog.jpg), maka file tersebut akan dihapus menggunakan function tambahan lainnya yaitu function `remove_file()`.

Berikut merupakan function utama `categorize()`:
```c
// function to categorize darat animal & air animal, then remove file that not both (in this case: frog.jpg)
void categorize(){
	DIR *dp;
	struct dirent *ep;
	char path[100] = "/home/nde/modul2/animal";
	dp = opendir(path);
	
	if(dp!=NULL) {
		while((ep = readdir(dp))){
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "darat")){
				char origin[100] = "modul2/animal/";
				
				strcat(origin, ep->d_name); 
				move_file("mv", origin, "modul2/darat");
			} 
			else if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "air")){
				char origin[100] = "modul2/animal/";
				
				strcat(origin, ep->d_name); 
				move_file("mv", origin, "modul2/air");
			} 
			else {
				if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0) {
					char origin[100] = "modul2/animal/";

					strcat(origin, ep->d_name); 
					remove_file("rm", origin);
				}
			}
		}
		(void) closedir (dp);
	} 
	else perror ("Cannot open the directory");
}
```

Lalu berikut merupakan function `move_file()` untuk memindahkan hewan darat&air sesuai nama filenya:
```c
// function to move file based on category
void move_file(char bash[], char filename[], char moveto[]) {
	pid_t child_id;
	int status;
	
    child_id = fork();
   
   if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
    if(child_id == 0){
        execlp(bash, bash, filename, moveto, NULL);
    }
    else{
        ((wait(&status))>0);
    }
}
```

Lalu berikut merupakan function `remove_file()` untuk menghapus file yang bukan darat maupun air (frog.jpg):
```c
// function to remove file/directory
void remove_file(char bash[], char filename[]) {
	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0) {
		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}
	if(child_id == 0){
		execlp(bash, bash, filename, NULL);
	}
	else{
		((wait(&status))>0);
	}
}
```

Pada main program pun dipanggil seperti berikut:
```c
	// CATEGORIZE DARAT & AIR, SISANYA REMOVE
	categorize();
```


#### d. Menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”
Pada bagian ini, saya membuat function `bye_bird()` yang berguna untuk meremove semua file yang bernama bird di dalam directory darat. Berikut detail isi fungsinya:
```c
// function to remove all of bird in darat dir
void bye_bird(){
	DIR *darat;
	struct dirent *ep;
	char path[100] = "/home/nde/modul2/darat";
	darat = opendir(path);

	if(darat != NULL) {
		while((ep = readdir(darat))) {
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "bird")){
				char origin[100] = "modul2/darat/";
				
				strcat(origin, ep->d_name); 
				remove_file("rm", origin);
		    }
		}
	(void) closedir (darat);
	} 
	else  perror ("Cannot open the directory");
}
```

Pada main program pun dipanggil seperti berikut:
```c
	// REMOVE ALL BIRD IN DARAT
	bye_bird();
```

#### e. Membuat file list.txt di folder “/home/[USER]/modul2/air” yang berisi hewan di directory air dengan format UID_[UID file permission]_Nama File.[jpg/png]

Pada bagian ini, saya membuat function `list_mazzeh()` yang berguna untuk membuat file `list.txt` dalam directory air yang berisikan list dengan format UID_[UID file permission]_Nama File.[jpg/png]. Dalam function ini juga diberikan conditional mengenai file permission dari file tersebut. Berikut detail isi fungsinya:
```c
// function to make list.txt
void list_mazzeh(){
	DIR *air;
	struct dirent *ep;
	char path[100] = "/home/nde/modul2/air";
	air = opendir(path);
	
	struct stat fs;
	int r;
	
	char *nde;
	nde=(char *)malloc(10*sizeof(char));
	nde=getlogin();
	
	FILE *list_txt = fopen("/home/nde/modul2/air/list.txt", "w");

	if(air!=NULL) {
		while((ep = readdir(air))) {
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strstr(ep->d_name, "jpg")) {
				char awal[100] = "modul2/air/";
				strcat(awal, ep->d_name);
				r = stat(awal, &fs);
				char readd = ' ';
				char writee = ' ';
				char executee = ' ';
				if( fs.st_mode & S_IRUSR ){
					readd = 'r';
				}
				if( fs.st_mode & S_IWUSR ){
					writee = 'w';
				}
				if( fs.st_mode & S_IXUSR ){
					executee = 'x';
				}
				fprintf(list_txt, "%s_%c%c%c_%s\n", nde, readd, writee, executee, ep->d_name);
			}
		} 
		(void) closedir (air);
	}
	else  perror ("Cannot open the directory");

	fclose(list_txt);
}
```

Pada main program pun dipanggil seperti berikut:
```c
	// MAKE LIST.TXT
	list_mazzeh();	
```


## Kendala
- Kesulitan dalam membuat rentang waktu 3 detik pada saat membuat directory darat lalu directory air
